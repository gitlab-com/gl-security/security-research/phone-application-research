## Vuln report to BambooHR

Initial contact was Michael Sanders, a product manager from BambooHR. This was
the support person that Ashley Jameson from PeopleOps was dealing with.

---
```
Date: Fri, 26 Jun 2020 10:04:14 -0500
Subject: Fwd: BambooHR Feedback
From: Mark Loveless <mloveless@gitlab.com>
To: redacted@bamboohr.com

Hi Michael!

I just received a forwarded copy of your email from Ashley Jameson. My name
is Mark Loveless and I am a security researcher at GitLab. This quarter,
one of the projects I worked on involved examining roughly half a dozen
mobile applications used by GitLab team members, including
BambooHR's mobile app. I've attached a copy of the internal report.

We are reporting this to you using the guidelines outlined in our
Responsible Disclosure policy (https://about.gitlab.com/security/disclosure/
<https://about.gitlab.com/security/disclosure/#external>), specifically the
final section (https://about.gitlab.com/security/disclosure/#external).

On a personal note, the mobile app is of limited use within our
organization and was not widely used - many employees were not aware the
app even existed. Most used their browser on the mobile phone for BambooHR
functions. However we have communicated internally for team members to not
use the application for now.

Please keep me apprised of any relevant data, feel free to forward this as
needed, and if you have any questions please let me know.

Thanks,

Mark

-- 
Mark Loveless - +01 817 368 6669
mloveless@gitlab.com | GitLab: @mloveless
Twitter: @simplenomad | Timezone: GMT-6

```
Attached: report.txt (this was an earlier version of the README)
---
```
From: Michael Sanders <msanders@bamboohr.com>
Date: Mon, 29 Jun 2020 08:58:48 -0700
Message-ID: <CALgftqsUpX5gWOyHOJaJf1+JkS-FMFX9LyYEGTvW=1_toUyrQg@mail.gmail.com>
Subject: Re: BambooHR Feedback
To: Mark Loveless <mloveless@gitlab.com>

Hi Mark, thank you for sharing the detailed findings.

Regarding your conclusion, what information did you base it on? Currently
we have no plans for a mobile app replacement.

> BambooHR is actively trying to hire a Progressive Web App developer, which
> suggests a mobile app replacement.

-- 
Michael Sanders | Product Manager
<redacted signature block>
```
---
```
Date: Mon, 29 Jun 2020 13:27:02 -0500
Subject: Re: BambooHR Feedback
From: Mark Loveless <mloveless@gitlab.com>
To: Michael Sanders <redacted@bamboohr.com>

I saw in your open positions you were looking for a Progressive Web App
developer. A common usage of that is to unify app development so that a
single base of code can be used for a native app, a web-based app, and a
mobile app. I was assuming the latter, perhaps more optimistically.

```
---
```
From: Michael Sanders <redacted@bamboohr.com>
Date: Thu, 9 Jul 2020 09:30:51 -0600
Subject: Re: BambooHR Feedback
To: Mark Loveless <mloveless@gitlab.com>

Hi Mark, thanks again for sharing your report. I shared that with the team
so they can review and assess your findings. Have a great day!
```
---
```
Date: Fri, 14 Aug 2020 12:04:59 -0500
Subject: Re: BambooHR Feedback
From: Mark Loveless <mloveless@gitlab.com>
To: Michael Sanders <redacted@bamboohr.com>

Hi Michael,

It's been a while since I've heard anything, was wondering if there has
been any progress on correcting the issues reported.

No pressure for right now, per our disclosure policy (
https://about.gitlab.com/security/disclosure/#external) I referenced in an
earlier email we intend to wait for 90 days before making anything public
beyond what we've already mentioned in the company handbook (see
https://about.gitlab.com/handbook/security/#mobile-applications).

If BambooHR has decided not move forward with adjustments to their mobile
app, that's fine - just let me know either way. As stated in the disclosure
policy, if you're actively working toward a fix, we are more than willing
to work with you for more than the standard 90 days before releasing
details.

Thanks,
Mark
```
---
```
From: Michael Sanders <redacted@bamboohr.com>
Date: Thu, 20 Aug 2020 17:04:28 -0600
Subject: Re: BambooHR Feedback
To: Mark Loveless <mloveless@gitlab.com>

Hi Mark, there have been some changes in our normal development activities
that have addressed some items in your list. I'll get a response to your
items and share it with you soon.

Thanks.
```
---
```
Date: Fri, 21 Aug 2020 13:53:59 -0500
Subject: Re: BambooHR Feedback
From: Mark Loveless <mloveless@gitlab.com>
To: Michael Sanders <iredacted@bamboohr.com>

Thanks for the update!
```
---

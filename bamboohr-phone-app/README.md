# BambooHR Phone App

This directory includes research notes, information, and any vulnerability findings involving the BambooHR Android app.

## Objectives

- Examine data stream for secure communication practices
- Examine data stream (including encrypted streams) to ensure the following:
    - Test for proper protections against XSS, injections, replay, etc
    - Test for unauthorized data upload
- Test authentication methods, including possible recovery methods
- Check libraries/SDKs for recent versions
- Test/eval locally stored data methods
- Perform real-world attacks

Tools used for analysis included apktool, jadx, mitmproxy, Wireshark, and grep ;-)

Attack testing used a variety of freeware tools, but mainly manual manipulation using mitmproxy.

## Details

The BambooHR apk version examined was 3.1.2. All dates are reflective of the time of investigation (June 2020).

### Data Stream - Encrypted

**Traffic Analysis:**

- As hsts is enforced on the server/cloud end, HTTPS is used for all communications. Cipher Suite: TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256 (0xc02b).
- Auth token has an expiration of one hour. Tokens are not reused if the app is restarted.
- Some of the locations allowed in the CSP appear to be related to authentication (e.g., connect.facebook.net) but this seems bizarre for an HR firm handling corporate accounts.
- Inline and Dynamic JavaScript is allowed per CSP.
- Firebase logging is uploading data such as phone type, OS, battery stats, and some potential unique numbers associated with the particular phone, as these changed if I changed phones but were consistent on each phone individually. These could have been unique to the platform itself, testing done with both iOS and Android.
- Copies of the corporate configuration settings are downloaded to the phone upon app startup, as is a list of all employees and their profile pictures (if uploaded to BambooHR). Unsure why this is the case unless it is intended to improve network performance via "caching". The photo part seems creepy though.

### Libraries/Packages/SDKs

Using the Android version of BambooHR 3.1.2 and decompiling the apk, a check of the libraries/packages/SDKs turned up the following data:

**Packages:**

All packages used were outdated or no longer actively supported. Since the code base was all over the place it was not easy to track down package versions, but a few examples are included:

- Fabric.io is version 1.4.1 from several years ago. Fabric.io was discontinued and all development was moved to Firebase.
- Facebook Stetho is version 1.0 from 2015. The current version from 2019 is version 1.5.1.
- GeniusScan SDK is several years out of date, based upon new features added to the product that are not in this version.

**Analysis:**

- This is entirely cut-and-paste code. Packages are included to support a single function.
- Poor naming conventions are used throughout, making the code hard to maintain.
- There is a tendency to use if/then statements solely based off of the version of the OS or the app to make logic decisions. 1 or 2 of these are fine, there are well over 600 in this entire app, again adding to the complexity of the code.
- BambooHR did not write the code, at least the initial version of it. They do not "own" the signing key for the app. This itself isn't unusual, but when you consider the app's coding logic and possible multiple contributors contributing to the included pcakages, maintenance would be challenging at best.
- A combination of older versions of certain packages, poor logic, and code complexity make this a prime candidate for abuse. If it were a Sudoku game, fine. This accesses GitLab RED data.

## Additional Areas

**MITM**

During testing roughly 20 MITM sessions were ran about 10 each against Android and iOS. In two of the Android cases, the MITM attack was successful. All 10 attempts with iOS were unsuccessful.

The testing consisted of using the program mitmproxy running on a computer running Ubuntu 20.04 with both a wired and wireless interface. The wired interface was connected to a router for Internet access, and the wireless interface was set up as a password-protected adhoc network. Each phone was placed in airplane mode, and then subsequently attached to the wireless adhoc network. Running the mitmproxy on the Ubuntu laptop and redirecting web traffic from the wireless interface to the wired interface allowed for potential interception. Using iOS all of the attempts failed. Using Android there were two successful MITM sessions, attempts three and four. Attempt five simply did not work and the phone froze up. After restarting the Android phone, there were no other cases of successes.

**Replay/XSS/etc**

After the MITM testing, mitmproxy certificates were loaded onto and "trusted" by the phone operating systems, and mitmproxy was used to examine encrypted streams as well as perform replay attacks, XSS, etc. While there were occasional inconsistent reactions (crashes etc), there was nothing repeatable. This was not a full penetration test of the app, it was to test a few scenarios just to see how the app reacted. While none of the exploit attempts worked, a few causes odd reactions that were often not repeatable. The Apple version of BambooHR seemed to be much more stable than the Android version, which is worth noting.

## Findings

- The code quality was poor (e.g. over 600 if/then statements used involving the Android build version). With poor code quality, there is a concern that minor changes to the app's environment (such as an upgrade) could easily introduce bugs and crashes, and some of these bugs could have security implications. For example Facebook Stetho has numerous null pointer dereferences in the first versions - running this version introduces numerous potential bugs. Tracing back in a given instance of an issue could be problematic at best since one would need to depend on having multiple versions of the phone's operating system to try and reproduce a code flaw - simply because of all of those if/then statements.
- The coder of the app is not BambooHR, but one of their customers - Moki Mobility. At least the initial coding (and the current app signing key) is Moki, BambooHR may be maintaining the code base as of now. The main issue with this is that Moki is a device management company, not a mobile app company, and this combined with the code quality itself makes the possibility of security issues much more likely.
- All included packages used in the app build were 5 years out of date. Some, such as Fabric, are no longer supported and have been replaced with Firebase.
- A successful MITM attack was conducted against the app, which allowed encrypted traffic to be decrypted. The MITM attacks were inconsistent (possibly related to the code quality issues mentioned above), but as the app handles RED data the certificate pinning should be in place to solidly prevent all MITM attacks consistently.

## Conclusions

It was recommended to PeopleOps (BambooHR DRI) that the BambooHR mobile app not be used by GitLab, any accesses to BambooHR from a mobile device should be done via browser, changes were made to the handbook to reflect this policy, and the information was announced to the company via Slack. The BambooHR SaaS application is fine, just the mobile app is the issue. Also note that most GitLab team members use the BambooHR via a web browser and do not use the mobile app as it does not provide the level of functionality required for day-to-day use in GitLab.

Security findings were reported via a Google Form doc on the bamboohr.com website, and also via PeopleOps through their support. I suspect that based upon feedback from PeopleOps we will not get timely resolution on this - partly based upon history and partly based upon the fact that BambooHR is actively trying to hire a Progressive Web App developer, which suggests a mobile app replacement.

## Vendor Notification

After some informal inquiries via PeopleOps, a contact was located in BambooHR and a report was sent. The reporting process is being documented [here](vuln01.md).

# Slack Phone App

This directory includes research notes, information, and any vulnerability findings involving the Slack phone app.

## Objectives

- Examine data stream for secure communication practices
- Examine data stream (including encrypted streams) to ensure the following:
    - Test for proper protections against XSS, injections, replay, etc
    - Test for unauthorized data upload
- Test authentication methods, including possible recovery methods
- Check libraries/SDKs for recent versions
- Test/eval locally stored data methods
- Perform real-world attacks

## Details

The Slack apk version examined was 20.04.20.0. All dates are reflective of the time of investigation (June 2020).

### Data Stream - Encrypted

**Traffic Analysis:**

TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256 used during all web communications as hsts is enforced. However there is no Content Security Policy. There is some risk from XSS as basic protections are not in place. Certificate pinning is enforced.

### Libraries/Packages/SDKs

Check for outdated libraries/packages/SDKs turned up the following data:

**Packages:**

All packages included in the apk are either current or one version out. None of the packages have known security issues.

**Analysis:**

All packages up to date.

## Additional Areas

- Some personal info is stored in local database files including configuration settings, and some data from private channels, but all of it is restricted by local permissions. This isn't great, but at least it is mitigated slightly.
- Slack resisted replays with older tokens and attempts to bypass auth. Straight MITM attacks failed.

## Findings

Local data storage protected by local permissions. Adequate web protections although XSS is possible. Authentication is done well.

## Conclusions

No reportable findings from a security perspective.

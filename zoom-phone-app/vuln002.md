***Vulnerability #2***

This supercedes vuln001, as the security@zoom.us email only recently came into existance. Same issue though, I used client Zoom 5 instead and only focused on the packages with security implications.

---
```
Date: Fri, 22 May 2020 17:31:32 -0500
Subject: Old packages used in Zoom apk
From: Mark Loveless <mloveless@gitlab.com>
To: security@zoom.us

Hello,

I took a look at the Android Zoom apk, and noticed a lack of updating of
packages. No packages except those directly developed by Zoom were up to
date. The following have potential security issues.

okhttp3 from Square
- Current version: 4.5.0 (released 2020-04-06)
- Version in Zoom apk: 3.7.0 (released 2017-04-15)
- Issues: Unsupported version, oldest version supported is 3.12.x. No
support for TLS 1.3. Vulnerable to CVE-2018-20200. Numerous crash fixes in
newer versions.

okio from Square
- Current version: 2.5.0 (released 2020-03-20)
- Version in Zoom apk: 1.12.0 (released 2017-04-11)
- Issues: Numerous crash fixes in newer versions, co-dependency with
vulnerable version of okhttp3.

apache from Apache
- Current version: 5.0
- Version in Zoom apk: 4.5
- Issues: Lack of support for RFCs 7230, 7231, 7235 and the security
implications therein.

 Jackson by FasterXML
- Current version: 2.11 (2020-04-26)
- Version in Zoom apk: 2.7 or earlier (2017, unsure of version but later
features in 2.8 suggest this version is pre-2.8)
- Issues: CVE-2017-7525, CVE-2017-15095, (possibly) CVE-2017-17485

Minimal JSON (com.eclipsesource.json)0.9.4 (2015-07-19, IOOBE, stack
overflow)
- Current version: 0.9.6 (2018-05-18)
- Version in Zoom apk: 0.9.4 (2015-07-19)
- Issues: IOOBE, stack overflow (no CVE)

I will assume many of the same packages are used in the iOS client as well.
I do understand this is probably not low priority, but as I am evaluating
mobile apps used by my employer, I am reporting this. In all fairness, ALL
packages, not just the ones with obvious security flaws, should be updated
to the latest supported versions.

Thank you,

Mark
-- 
Mark Loveless - +01 817 368 6669
mloveless@gitlab.com | GitLab: @mloveless
Twitter: @simplenomad | Timezone: GMT-6
```

---

```
Date: Fri, 22 May 2020 22:31:47 +0000
From: Security <security@zoomus.zendesk.com>
Reply-To: Security <security+id5769470@zoomus.zendesk.com>
To: Mark Loveless - Sr Security Researcher <mloveless@gitlab.com>
Subject: [Request received] Old packages used in Zoom apk


# Please type your reply above this line #

Mark,

Thank you for contacting Zoom Support.=20

Your request (#5769470) has been received, and is being reviewed by our sup=
port staff.

We strive to answer all e-mail support requests as soon as possible. In the=
 interim, please feel free to take a look at our FAQs at our Support Center=
 (https://support.zoom.us) if you haven't already. You can download our lat=
est software update at https://zoom.us/download. You can also attend our fr=
ee weekly on-boarding training at https://zoom.us/livetraining

To add additional comments, reply to this email or follow the link above to=
 review your ticket:

```
---
```
Date: Tue, 08 Sep 2020 15:19:22 +0000
From: "Adam Ruddermann (Security)" <security@zoomus.zendesk.com>
Reply-To: Security <security+id4449403@zoomus.zendesk.com>
To: Mark Loveless - Sr Security Researcher <mloveless@gitlab.com>
Subject: Re: iOS and Android apps

Adam Ruddermann, Sep 8, 2020, 10:19 CDT

Hi Mark,

Let me introduce myself - I'm Zoom's new Head of Vulnerability Management 
and Bug Bounty. Thanks for reaching out and your patience with the the delay
in response from our end.

In the latest version of our Android application, it appears we no longer
use okhttp, fasterxml, or Minimal JSON. Additionally, we have recently been
working to get all of our third-party dependencies up to date across all of
our clients.

Please let us know if you see differently and please do continue to reach
out with any security concerns or potential vulnerabilities. We recently
created a new page at https://zoom.us/docs/ent/h1.html where security
vulnerabilities via our HackerOne VDP. This will allow our security 
engineers to quickly assess and respond to reports to prevent the delay you
experienced here in the future.

If you'd ever like to chat, please feel free to reach out to me at 
<redacted>@zoom.us directly!

Thanks and all the best,
rudd
```
---
```
Date: Tue, 8 Sep 2020 16:11:41 -0500
Subject: Re: iOS and Android apps
From: Mark Loveless <mloveless@gitlab.com>
To: Security <security+id4449403@zoomus.zendesk.com>

Hi Adam,

I did look over the Android apk briefly, and was pleased to see you have
updated things quite a bit. Thank you for letting me know.

Mark

```
---

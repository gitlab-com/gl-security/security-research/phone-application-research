***Vulnerability #3***

Local storage of E2E encryption keys and client PEM in a SQLite database. The data appeared encryted. This should be stored in a more secure fashion, such as chip-based secure storage.

Reported 20200522 [here](https://github.com/zoom/zoom-e2e-whitepaper/issues/5#issue-623475741).

Vendor responded 20200529 [here](https://github.com/zoom/zoom-e2e-whitepaper/issues/5#issuecomment-636189248), stating it will be addressed in a future release.


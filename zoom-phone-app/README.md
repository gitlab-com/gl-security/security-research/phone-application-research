# Zoom Phone App

This directory includes research notes, information, and any vulnerability findings involving the Zoom Android app.

## Objectives

- Examine data stream for secure communication practices
- Examine data stream (including encrypted streams) to ensure the following:
    - Test for proper protections against XSS, injections, replay, etc
    - Test for unauthorized data upload
- Test authentication methods, including possible recovery methods
- Check libraries/SDKs for recent versions
- Test/eval locally stored data methods
- Perform real-world attacks

## Details

The Zoom apk version examined was 5.02. All dates are reflective of the time of investigation (May 2020).

### Data Stream - Encrypted

**Traffic Analysis:**

Traffic was divided between DNS queries; SSL for authentication/setup/key exchange; and a proprietary streaming protocol on UDP port 8801 that is encrypted. This was not limited to Android  traffic - it should be noted that iOS, MacOS, and Linux clients used these same communications.

SSL traffic defaulted to `TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256` on all platforms. TLSv1.2 was used as well. This was the same on all platforms. While it was possible to force use of the browser instead of the client on both the Mac and Linux platforms, the same could not be said for Android or iOS - I was unable to make this happen.

Not only was certificate pinning used, starting with version 5.0 of the app there is _additional_ client-side verification of the certification, so using MITM techniques with my own certificates install and "trusted" by the device does not allow for bypassing of certificate pinning.

### Libraries/Packages/SDKs

Check for outdated libraries/packages/SDKs turned up the following data:

**Packages:**

No packages except those directly developed by Zoom were up to date. The following have potential security issues.

okhttp3 from Square
- Current version: 4.5.0 (released 2020-04-06)
- Version in Zoom apk: 3.7.0 (released 2017-04-15)
- Issues: Unsupported version, oldest version supported is 3.12.x. No support for TLS 1.3. Vulnerable to CVE-2018-20200. Numerous crash fixes in newer versions.

okio from Square
- Current version: 2.5.0 (released 2020-03-20)
- Version in Zoom apk: 1.12.0 (released 2017-04-11)
- Issues: Numerous crash fixes in newer versions, co-dependency with vulnerable version of okhttp3.

apache from Apache
- Current version: 5.0
- Version in Zoom apk: 4.5
- Issues: Lack of support for RFCs 7230, 7231, 7235 and the security implications therein.

 Jackson by FasterXML
- Current version: 2.11 (2020-04-26)
- Version in Zoom apk: 2.7 or earlier (2017, unsure of version but later features in 2.8 suggest this version is pre-2.8)
- Issues: CVE-2017-7525, CVE-2017-15095, (possibly) CVE-2017-17485

Minimal JSON (com.eclipsesource.json)0.9.4 (2015-07-19, IOOBE, stack overflow)
- Current version: 0.9.6 (2018-05-18)
- Version in Zoom apk: 0.9.4 (2015-07-19)
- Issues: IOOBE, stack overflow (no CVE)

**Analysis:**

Despite the presence of Apache and Okhttp3, X509 certificate handling and SSL/TLS is being done by proprietary Zoom code. There is support for both Ice Cream Sandwich and Jelly Bean in their Android code.

## Addition Areas

**Local Storage:**

The E2E encryption keys and client-side PEM are stored locally on the client in a sqlite database. These entries contain encrypted data. Deleting them and restarting the app recreated them with different values. The permissions are decent but the database is still wide open.

## Findings

- Outdated libraries used in the Android app build. Submitted bug report early on since this was deemed fairly critical yet easily correctable. See [vuln002.md](vuln002.md) for details. E2E encryption keys and client-side PEM are stored in a local SQLite database, it was reported to Zoom via an issue on their Github encryption documentation that more secure locations (e.g. hardware-based security chips). See [vuln003.md](vuln003.md).

## Conclusions

The vast majority of security flaws related to the older packages are dependent on being vulnerable to XSS or MITM attacks, and these are mitigated against in the app. In September 2020, the issues with older packages were resolved either by updating or removing the old packages. The issue with the encrypted keys being stored in a local database is slightly disturbing, however a reasonable attack scenario is unlikely and therefore we can wait until the solution is eventually implemented. The Zoom phone application is deemed safe to use.

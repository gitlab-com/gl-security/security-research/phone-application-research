***Issues reported to Expensify***

First reporting of security issues to Expensify.

---
```
Date: Mon, 22 Jun 2020 10:14:41 -0500
Subject: Issues with Expensify Mobile App
From: Mark Loveless <mloveless@gitlab.com>
To: security@expensify.com

Hello,

My name is Mark Loveless and I am a security researcher at GitLab. I was doing
a bit of due diligence on various mobile apps our team members use, and as we
use Expensify I checked out the mobile app. I found a few issues I'd like to
report.

I looked at version 8.5.10.12. During examination of the apk, I noticed that 
most packages included in the apk were out of date by an average of at least 
two years. This brings up concerns from a security perspective. The major 
package I wanted to point out was the following:

okhhtp/3.10.0 - The current version is 4.7.2, 3.10.0 is no longer supported. 
This means that security vulnerabilities found in okhttp will not be patched 
in the current version. With okhttp being as out of date as it is, there are 
potential issues as CVE-2018-20200 was not addressed in this version, and 
there are numerous improvements including the addition of RFCs that help 
security in more recent versions.

There are other packages tied to okhttp such as okio and kotlin, both of which
are well over two years out of date. This is concerning as these packages 
handle the majority of the network communications.

The main issue I had was the use of a package from Branch. To quote from 
Branch's website "Branch's People-Based Attribution uses deterministic web 
cookie + device ID pairs to match touchpoints from every channel with 
conversions on any platform. We empower you to eliminate the ambiguity of 
fingerprint-based attribution and unify fragmented data to show you each 
customer's full journey." This particular package is gathering enough personal
data to determine the identity of the user of the application without their 
consent. 

Expensify's Privacy Policy states that you do not gather personal data to 
share with Partnering Companies, however you are sharing enough data that 
Branch can uniquely track individuals.

I used the Apple iPhone with Expensify v.8.5.10.12, and by using the mitmproxy
tool was able to inspect the encrypted communications, and I've attached two
screenshots from traffic going to branch.io that illustrate this. 

In the first screenshot note that the "ad_tracking_enabled" is marked "true" -
something I did not give consent to. Additionally there are values that 
uniquely identify my phone from all other phones (I've blanked out a few for
privacy reasons). As Branch works with numerous organizations, this is how
they are able to uniquely identify me per their website - by pulling data from
multiple sources.

We are reporting this to you under our Disclosure Policy which is defined 
here: https://about.gitlab.com/security/disclosure/. In particular, the last 
section entitled "Disclosure Guidelines for Vulnerabilities in 3rd Party 
Software" applies here.

If you have any questions or need more detail please feel free to reach out
and contact me.

Thank you,

Mark

-- 
Mark Loveless - +01 817 368 6669
mloveless@gitlab.com | GitLab: @mloveless
Twitter: @simplenomad | Timezone: GMT-6

Attachments: [expensify-branch-io-data-gathering-01.png](expensify-branch-io-data-gathering-01.png)
[expensify-branch-io-data-gathering-02.png](expensify-branch-io-data-gathering-01.png)
```
---
```
From: Carlos Alvarez <redacted@expensify.com>
Date: Tue, 23 Jun 2020 11:57:03 -0700
Subject: Re: Issues with Expensify Mobile App
To: Mark Loveless <mloveless@gitlab.com>
Cc: security@expensify.com

Hi Mark, thanks a lot for bringing this up to our attention. We're revising
our policy for mobile library updates to bring it in line with the rest of
our updates practices. We'll be upgrading these libraries in upcoming
releases and after looking at the CVE you mentioned we determined it's not
a high risk vulnerability for us and thus won't expedite a release with the
update.

Last, we've contacted Branch about the "ad_tracking_enabled " data our app
is sending, to make sure the functionality we're using from Branch is
strictly for deep linking and to disable anything else that would fall
outside our privacy policy.

Cheers,
Carlos
```
---
```
Date: Tue, 23 Jun 2020 15:07:00 -0500
Subject: Re: Issues with Expensify Mobile App
From: Mark Loveless <mloveless@gitlab.com>
To: Carlos Alvarez <redacted@expensify.com>
Cc: security@expensify.com

Carlos,

Thanks for the update. Our compliance folks here are wondering about a
potential release date for these issues. Truthfully the main thing we are
concerned with is the issue with Branch, and it does seem like that might
be the easiest one to fix.

Please keep us posted, and thanks for the quick response!

Mark
```
---
```
From: Carlos Alvarez <redacted@expensify.com>
Date: Wed, 24 Jun 2020 15:12:50 -0700
Subject: Re: Issues with Expensify Mobile App
To: Mark Loveless <mloveless@gitlab.com>
Cc: security@expensify.com

Hi Mark, I don't have an ETA yet but I can provide one as soon as we have
it.

Carlos
```
---
```
From: Carlos Alvarez <redacted@expensify.com>
Date: Mon, 29 Jun 2020 17:03:28 -0700
Subject: Re: Issues with Expensify Mobile App
To: Mark Loveless <mloveless@gitlab.com>
Cc: security@expensify.com

Hello again Mark, just wanted to let you know that we expect to have an
updated release of the app by the end of next month.

Cheers,
Carlos
```
---
```
Date: Tue, 30 Jun 2020 13:37:57 -0500
Subject: Re: Issues with Expensify Mobile App
From: Mark Loveless <mloveless@gitlab.com>
To: Carlos Alvarez <redacted@expensify.com>

Hi Carlos, this is good news, glad you are moving forward quickly on this.
Looking forward to seeing the updated release.

Mark
```
---
```
Date: Fri, 21 Aug 2020 16:19:19 -0500
Subject: Re: Issues with Expensify Mobile App
From: Mark Loveless <mloveless@gitlab.com>
To: Carlos Alvarez <redacted@expensify.com>

Hi Carlos, any word on the updated release? It looks like 8.5.13.5 hasn't
updated older libraries (Okhttp3 is still on 3.10.0), and it looks like
Branch is still in use. I haven't had the chance to look at traffic yet,
but thought I would ask you for an update on the progress.

Mark
```
---
```
From: Carlos Alvarez <redacted@expensify.com>
Date: Tue, 1 Sep 2020 11:32:20 -0700
Subject: Re: Issues with Expensify Mobile App
To: Mark Loveless <mloveless@gitlab.com>

Hi Mark, sorry for the late reply, we just published an internal release
candidate with the branch fix that is going through final QA tests and
should be live in a week or so.

Cheers,
Carlos
```
---
```
Date: Tue, 1 Sep 2020 13:33:13 -0500
Subject: Re: Issues with Expensify Mobile App
From: Mark Loveless <mloveless@gitlab.com>
To: Carlos Alvarez <redacted@expensify.com>

Awesome, thanks for the update!
```
---

# Expensify Phone App

This directory includes research notes, information, and any vulnerability findings involving the Expensify phone app.

## Objectives

- Examine data stream for secure communication practices
- Examine data stream (including encrypted streams) to ensure the following:
    - Test for proper protections against XSS, injections, replay, etc
    - Test for unauthorized data upload
- Test authentication methods, including possible recovery methods
- Check libraries/SDKs for recent versions
- Test/eval locally stored data methods
- Perform real-world attacks

## Details

The Expensify apk version examined was 8.5.10.12. All dates are reflective of the time of investigation (June 2020).

### Data Stream - Encrypted

**Traffic Analysis:**

TLS with is TLS_AES_256_GCM_SHA384.

The CSP policy sets up X-Frame-Options via frame-ancestors to prevent 
clickjacking, but is otherwise sparse. XSS protection is in place. HSTS set up
correctly. Cookies use Secure flag and session cookies use the HTTPOnly flag.
Overall the security items used for Expensify resources is fairly good.

### Libraries/Packages/SDKs

Check for outdated libraries/packages/SDKs turned up the following data:

**Packages:**

Major packages:

okhttp/3.10.0 - current version is 4.7.2, 3.10.0 is no longer supported. CVEs
include CVE-2018-20200 (disputed, and I agree with the dispute). Only 3.12
supports backport of new security issues and will be fully deprecated Dec 2021.
Numerous issues including some DNS caching issues as well. Lack of support for
newer advances in HTTP protocols and newer Internet RFCs.

okio/?? - version unknown but since linked to okhttp/3.10.0 and based upon
contents about two years out of date.

kotlin/1.3.10 - current version is 1.4-M2. Requires at least 1.3.71 for Okhttp
4.7.2.

Most other packages are out of date by at least 2 years, some by as much as 4
years.

Questionable packages:

Urbanairship (aka airship) and app-measurement - These packages do data
gathering based upon app usage, although the main thing they collect that is
questionable is the unique hardware ID of the device.

branch.io - This company is a deep link analysis company, and gathers a lot of
data from the device that could be used to uniquely identify the user of the
phone. Attached are two screen shots (captured using mitmproxy) that show how
"ad_tracking_enabled" is marked "true" and how numerous pieces of unique
identifying data is captured (I blanked out a few values for privacy reasons).

[expensify-branch-io-data-gathering-01.png](expensify-branch-io-data-gathering-01.png)
[expensify-branch-io-data-gathering-02.png](expensify-branch-io-data-gathering-02.png)

## Additional Areas

Certificate pinning is used effectively, unable to MITM without using special
certificates. Replay attacks and other token manipulation failed.

## Findings

The usage of Branch is the most severe issue, as it invites personal tracking.
Older packages used in the application is also an issue, albeit not as severe.

## Conclusions

There is nothing that would prevent the usage of this application within the
GitLab environment. The issues have been reported ([vuln01.md](vuln01.md)) to
Expensify via security@expensify.com. As of Sept 2020 the branch.io issue has been corrected.


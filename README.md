# Phone App Research

This directory includes research notes, information, and any vulnerability related to phone applications used by GitLab team members.

**About this research**

In an effort to ensure the safety and security of GitLab, we are looking at our use of mobile phone applications to ensure they are safe when accessing GitLab data. Mobile devices are allowed to access GitLab by team members, and to ensure safety measures are in place to protect the data, we are ensuring that this communication is safe, data is protected, and the team members are not introducing potential security issues that would affect themselves or GitLab.

Mobile apps usually connect up and access cloud-based resources using the same protocols that web browsers do. However unless the mobile app developer has taken proper precautions, a typical mobile app does not offer up the same built-in security features that a web browser would. So the following items are checked:
- Basics are covered - encrypted communications, certificate pinning, if Bluetooth it is done securely, data storage on cloud/phone, no hard-coded passwords.
- Ensure encryption is strong.
- No unintended information leaks. This includes excessive permissions, or unintended data in uploads.
- Packages/libraries/SDK included are up-to-date and secure.
- Security elements cannot be bypassed.
- Stored data integrity is assured.
- Design real-world attack scenarios, and attempt them.

**Current efforts**

- [Zoom phone application](zoom-phone-app). 

- [Slack phone application](slack-phone-app).

- [BambooHR phone application](bamboohr-phone-app).

- [Expensify phone application](expensify-phone-app).  

